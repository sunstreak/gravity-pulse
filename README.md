# README #

### How do I review the code? ###
* The bulk of meaningful code is in the "core" directory. Other directories are libgdx framework.

### What is this repository for? ###

* Summary: Physics game based on radial gravity. Pilot a ship through a field of planets and debris by using clicks/taps to create gravity pulses.
* This is an alpha version. Gameplay physics are largely implemented. Game design incomplete, and UI framework incomplete.

### How do I get set up? ###

* Summary of set up: This is a Gradle project that requires libgdx. It is most functional when run on the desktop, but can run on Android devices with some known issues.
* Dependencies: Libgdx and JSON (included).

### Who do I talk to? ###

* The project is presently inactive. The maintaining dev is dylhunn. You may also contact the other devs, sidharthkapur and sunstreak.