package app.sunstreak.gravity.util;

public class GravityWellException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6748223451493075519L;

	public GravityWellException (String message) {
		super(message);
	}
}
