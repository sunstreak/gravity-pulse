package app.sunstreak.gravity.util;

/**
 * This class provides file paths for all of the assets used by the application.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */
public class Resources {

	private Resources() { /* Don't instantiate me! */
	}

	public static final String PROGRESS_FILE_NAME = "Progress.json";

	public static final String LEVELS_PREFIX = "levels/";
	public static final String LEVELS_FILE_NAME = LEVELS_PREFIX + "Levels.json";

	public static class images {
		private images() {
			/* Don't instantiate me! */
		}

		public static final String PREFIX = "images/";

		public static final String DEBRIS = PREFIX + "rock.gif";

		public static final String RIPPLE = PREFIX + "smalltransripple.png";

		public static final String MEDICINE = PREFIX + "medicinesmall.png";
		
		public static final String FREE_MOON = PREFIX + "freemoon.png";
		public static final String EARTH = PREFIX + "gate.png";
		
		public static final String REPULSOR = PREFIX + "repulsorsmall1.png";

		public static final String[] SHIP = { PREFIX + "ship.png",
				PREFIX + "ship1r.png", PREFIX + "ship2r.png",
				PREFIX + "ship3r.png" };

		public static final String WALL = PREFIX + "forcelinetexture.png";

		public static final String WORMHOLE = PREFIX + "spiralsmall.png";

		public static final String PAUSED = PREFIX + "paused.png";
		public static final String PAUSE = PREFIX + "pause.png";
		public static final String BACKGROUND = PREFIX + "space1_final.jpg";
		public static final String ARROW = PREFIX + "arrow.png";

		public static final String SQUARE = PREFIX + "square.png";
		public static final String UI_SKIN = PREFIX + "uiskin.json";
		public static final String SQUARE_CHECK = PREFIX + "squareCheck.png";
	}
}
