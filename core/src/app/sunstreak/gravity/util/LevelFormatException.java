package app.sunstreak.gravity.util;

public class LevelFormatException extends GravityWellException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LevelFormatException (String message) {
		super(message);
	}
}
