package app.sunstreak.gravity.frontend;

import app.sunstreak.gravity.backend.GravityMainGame;
import app.sunstreak.gravity.backend.Progress;
import app.sunstreak.gravity.util.Resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
/**
 * This class is the Screen that allows the user to view their highscore from each
 * level as well as their total high score from all of the levels combined using
 * a table on a stage.
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */
public class HighScoreScreen implements Screen {
	private final GravityMainGame g;
	private final Table container;
	private final SpriteBatch batch;
	private final Stage stage;
	private Skin skin;
	public static final Texture BACKGROUND = new Texture(
			Resources.images.BACKGROUND);

	/**
	 * Instantiate the screen that renders the High Score Screen showing high scores
	 * @param game the GravityMainGame in which the HighScoreScreen will be constructed in
	 */
	public HighScoreScreen(GravityMainGame game) {
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		batch = new SpriteBatch();
		this.g = game;
		skin = new Skin(Gdx.files.internal(Resources.images.UI_SKIN));
		container = new Table();
		container.pad(50);
		stage.addActor(container);
		container.setFillParent(true);
		container.defaults().width(300).height(50);
		drawHighScores();
		
		TextButton toMenu = new TextButton("Main Menu", skin);
		toMenu.getStyle().up = (new TextureRegionDrawable(new TextureRegion(
				new Texture(Resources.images.SQUARE))));
		toMenu.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				g.showMainMenu();
			}
		});
		container.add(toMenu);
	}
	
	/**
	 * Draws the high score components in the stage
	 */
	public void drawHighScores()
	{
		List<String> list = new List<String>(skin);
		Label text = new Label("High Scores", skin);
		text.setAlignment(Align.center);
		container.add(text).colspan(5);
		container.row();
		String[] highScores = Progress.getHighScores();
		list.setItems(highScores);
		ScrollPane scrollPane2 = new ScrollPane(list, skin);
		scrollPane2.setSize(200, 400);
		container.add(scrollPane2);
		container.row();
		Label highScore = new Label("Total Score: " + Progress.getTotalScore(),
				skin);
		highScore.setAlignment(Align.center);
		container.add(highScore);
		container.row();
	}
	/**
	 * On each render iteration, draw the menu and ask all elements to act.
	 */
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glClearColor(0, 0, 0, 0);
		stage.act(Gdx.graphics.getDeltaTime());
		batch.begin();
		batch.draw(BACKGROUND, 0, 0);
		batch.end();
		stage.draw();
	}

	/**
	 * System method; do nothing
	 */
	@Override
	public void resize(int width, int height) {
	}

	/**
	 * System method; do nothing
	 */
	@Override
	public void show() {
	}

	/**
	 * System method; do nothing
	 */
	@Override
	public void hide() {
	}

	/**
	 * System method; do nothing
	 */
	@Override
	public void pause() {
	}

	/**
	 * System method; do nothing
	 */
	@Override
	public void resume() {
	}

	/**
	 * Called manually to dispose of objects that might not otherwise be garbage
	 * collected.
	 */
	@Override
	public void dispose() {
		stage.dispose();
		batch.dispose();
	}

}
