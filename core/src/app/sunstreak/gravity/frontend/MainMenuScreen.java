package app.sunstreak.gravity.frontend;

import app.sunstreak.gravity.backend.GravityMainGame;
import app.sunstreak.gravity.util.Resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
/**
 * This class is the Screen that allows the user to begin their adventure in the game by 
 * going on to select levels or view their high score.
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */
public class MainMenuScreen implements Screen {
	private final GravityMainGame g;
	private final Table container;
	private final SpriteBatch batch;
	private final Stage stage;
	private Skin skin;
	public static final Texture BACKGROUND = new Texture(
			Resources.images.BACKGROUND);

	/**
	 * Instantiate the screen that renders the MainMenu to let users begin playing
	 * @param game the GravityMainGame in which the MainMenu will be shown
	 */
	public MainMenuScreen(GravityMainGame game) {
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		batch = new SpriteBatch();
		this.g = game;
		skin = new Skin(Gdx.files.internal(Resources.images.UI_SKIN));
		container = new Table();
		container.pad(50);
		stage.addActor(container);
		container.setFillParent(true);
		container.defaults().width(100).height(50);
		drawItems();
		
		
	}
	
	/**
	 * Draw the items on main menu screen
	 */
	public void drawItems()
	{
		Label text = new Label("Gravity Pulse", skin);
		text.setAlignment(Align.center);
		container.add(text);
		container.row();
		ImageButton toLevelSelect = new ImageButton(new TextureRegionDrawable(
				new TextureRegion(new Texture(Resources.images.ARROW))));
		toLevelSelect.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				g.openLevelSelector();
				// new LevelSelectorScreen(g);
			}
		});
		TextButton toHighScore = new TextButton("High Scores", skin);
		toHighScore.getStyle().up = (new TextureRegionDrawable(
				new TextureRegion(new Texture(Resources.images.SQUARE))));
		toHighScore.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				g.showHighScoreScreen();
			}
		});
		container.add(toLevelSelect);
		container.row();
		container.add(toHighScore);
	}
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glClearColor(0, 0, 0, 0);
		stage.act(Gdx.graphics.getDeltaTime());
		batch.begin();
		batch.draw(BACKGROUND, 0, 0);
		batch.end();
		stage.draw();
	}

	/**
	 * System method; do nothing
	 */
	@Override
	public void resize(int width, int height) {
	}

	/**
	 * System method; do nothing
	 */
	@Override
	public void show() {
	}

	/**
	 * System method; do nothing
	 */
	@Override
	public void hide() {
	}

	/**
	 * System method; do nothing
	 */
	@Override
	public void pause() {
	}

	/**
	 * System method; do nothing
	 */
	@Override
	public void resume() {
	}

	/**
	 * Called manually to dispose of objects that might not otherwise be garbage
	 * collected.
	 */
	@Override
	public void dispose() {
		batch.dispose();
		stage.dispose();
	}

}
