package app.sunstreak.gravity.frontend;

import app.sunstreak.gravity.actor.Gate;
import app.sunstreak.gravity.actor.Ship;
import app.sunstreak.gravity.actor.SpaceActor;
import app.sunstreak.gravity.backend.GravityMainGame;
import app.sunstreak.gravity.backend.Grid;
import app.sunstreak.gravity.backend.Level;
import app.sunstreak.gravity.backend.SimplePhysics;
import app.sunstreak.gravity.util.Resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * This class is the Screen that manages the rendering of the Level. It
 * interacts with Grid to draw all of the SpaceActors on every frame, and
 * handles the essential Camera and Viewport functions. This is the primary
 * gameplay display logic class, and has by necessity significant state; reorder
 * calls with care.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */
public class LevelHandlerScreen implements Screen {

	private final Grid grid;

	private final GravityMainGame game;
	private final OrthographicCamera camera;
	private final SpriteBatch batch;

	private final Compass compass;
	private final PauseButton pauseButton;
	private final Texture pausedTexture;
	public final Texture BACKGROUND_TEXTURE;

	private Ship followedActor;
	private SpaceActor targetActor;

	private boolean isTouched;
	private boolean paused;

	/**
	 * Instantiate the screen that renders the Level and all of the SpaceActors
	 * in the Grid.
	 * 
	 * @param level
	 *            the Level to display
	 * @param game
	 *            the GravityMainGame in which the Level will be played
	 */
	public LevelHandlerScreen(Level level, GravityMainGame game) {
		this.game = game;
		// width and height of the camera view
		grid = new Grid(level);
		camera = new OrthographicCamera(SimplePhysics.SCRWIDTH,
				SimplePhysics.SCRHEIGHT);
		batch = new SpriteBatch();

		pauseButton = new PauseButton(this);
		paused = false;
		pausedTexture = new Texture(Resources.images.PAUSED);
		configureInput(); // pause and gameplay input layers
		compass = new Compass();

		// draw the background
		BACKGROUND_TEXTURE = new Texture(Resources.images.BACKGROUND);
		setSpecialActors();
		Gdx.gl.glClearColor(0, 0, 0, 0);
	}

	/**
	 * Called many times each second to render all the objects in the Level.
	 * 
	 * @param deltaTime
	 *            time elapsed since render was last called
	 */
	@Override
	public void render(float deltaTime) {
		// are we paused?
		if (checkPausedState())
			return;
		if (gameOver())
			return;
		// make camera follow followedActor
		lerpCamera();
		// Boilerplate (clears the screen)
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		// on touch, generate gravity by calling touch() with the touchposition
		if (isTouched)
			generateGravityOnTouch();
		// All drawing must occur between begin() and end()
		batch.begin();
		drawElements();
		batch.end();
		batch.setProjectionMatrix(camera.combined);
		camera.update();
		// next frame with elapsed time
		grid.tick(1 / deltaTime);
	}

	/**
	 * Called by the input layer when a touch begins.
	 */
	public void startTouch() {
		isTouched = true;
	}

	/**
	 * Called by the input layer when a touch ends.
	 */
	public void stopTouch() {
		isTouched = false;
		grid.stopTouch();
	}

	/**
	 * If the screen was touched, generate gravity at the touch position.
	 */
	private void generateGravityOnTouch() {
		Vector3 touchPos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
		camera.unproject(touchPos);
		grid.touch(new Vector2(touchPos.x, touchPos.y));
	}

	/**
	 * Is the game currently paused?
	 * 
	 * @return whether the game is paused
	 */
	public boolean isPaused() {
		return paused;
	}

	/**
	 * Check whether the game is paused. If so, check to see if the screen was
	 * touched, which should unpause the game.
	 * 
	 * @return whether the game is still paused
	 */
	private boolean checkPausedState() {
		if (paused) {
			if (isTouched) {
				paused = false;
				return false;
			} else
				return true;
		}
		return false;
	}

	/**
	 * Determine whether the game ended. If so, end the game.
	 * 
	 * @return whether the game ended
	 */
	private boolean gameOver() {
		switch (followedActor.getGameStatus()) {
		case WIN:
			// TODO: naive score algorithm
			int livesRemaining = (followedActor.getHealthLevel() + 1);
			game.win(livesRemaining, grid.getGameTimeElapsed());
			return true;
		case LOSS:
			game.lose();
			return true;
		case IN_PROGRESS:
			return false;
		default:
			return false;
		}
	}

	/**
	 * Draw the background, then all SpaceActors, then the camera and compass.
	 */
	private void drawElements() {
		// draw the background
		BACKGROUND_TEXTURE.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
		final int tileCount = 1000;
		batch.draw(BACKGROUND_TEXTURE, -BACKGROUND_TEXTURE.getWidth()
				* tileCount / 2, -BACKGROUND_TEXTURE.getHeight() * tileCount
				/ 2, BACKGROUND_TEXTURE.getWidth() * tileCount,
				BACKGROUND_TEXTURE.getHeight() * tileCount, 0, tileCount,
				tileCount, 0);

		// draw all actors
		for (SpaceActor s : grid.getSpaceActors()) {
			s.drawSelf(batch);
		}

		compass.drawSelf(batch, camera, targetActor.getPosition());
		pauseButton.drawSelf(batch, camera);
	}

	/**
	 * Linear interpolation; use a value between zero and one. Linear
	 * interpolation will make the camera follow the followedActor while
	 * allowing it to shift within the frame. A value of zero for lerp makes the
	 * camera not follow the actor, and a value of one makes the camera follow
	 * exactly. Values in between produce "bounce."
	 */
	private void lerpCamera() {
		final float lerp = 0.2f;
		// Make the camera follow the followedActor
		Vector3 followedActorPosition = new Vector3(
				followedActor.getPosition(), 0);
		// This mutates the camera's position.
		camera.position.lerp(followedActorPosition, lerp);
	}

	/**
	 * The input multiplexer handles multiple inputprocessors. The order in
	 * which they are added matters: as soon as one processes the input (returns
	 * true), the input is not passed down to the next processor.
	 */
	private void configureInput() {
		InputMultiplexer multiplexer = new InputMultiplexer();
		multiplexer.addProcessor(pauseButton);
		multiplexer.addProcessor(new GameplayInputProcessor(this));
		Gdx.input.setInputProcessor(multiplexer);
	}

	/**
	 * The camera follows the followedActor; the compass points toward the
	 * targetActor
	 */
	private void setSpecialActors() {

		for (SpaceActor s : grid.getSpaceActors()) {
			if (s instanceof Ship)
				followedActor = (Ship) s;
			if (s instanceof Gate)
				targetActor = s;
		}
	}

	/**
	 * System method; do nothing
	 */
	@Override
	public void resize(int width, int height) {
	}

	/**
	 * System method; do nothing
	 */
	@Override
	public void show() {
	}

	/**
	 * System method; do nothing
	 */
	@Override
	public void hide() {
	}

	/**
	 * Called automatically by the system when the game should pause, typically
	 * when the application loses focus. Also called manually to pause the game.
	 */
	@Override
	public void pause() {
		// TODO Shouldn't pauseButton.drawSelf be called instead of this?
		Vector3 pl = new Vector3();
		pl.x = SimplePhysics.SCRWIDTH / 2;
		pl.y = SimplePhysics.SCRHEIGHT / 2;
		camera.unproject(pl);
		batch.begin();
		batch.draw(pausedTexture, pl.x - (pausedTexture.getWidth() / 2), pl.y
				- (pausedTexture.getHeight() / 2));
		batch.end();
		paused = true;
	}

	/**
	 * Called automatically or manually each time the game should resume.
	 */
	@Override
	public void resume() {
		paused = false;
	}

	/**
	 * Called manually to dispose of objects that might not otherwise be garbage
	 * collected.
	 */
	@Override
	public void dispose() {
		// never called automatically
		grid.dispose();
		BACKGROUND_TEXTURE.dispose();
		batch.dispose();
	}
}