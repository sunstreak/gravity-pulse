package app.sunstreak.gravity.frontend;

import org.json.JSONException;

import app.sunstreak.gravity.backend.GravityMainGame;
import app.sunstreak.gravity.backend.Progress;
import app.sunstreak.gravity.util.GravityWellException;
import app.sunstreak.gravity.util.Resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
/**
 * This class is the Screen that allows the user to select which of the following
 * level to play using a table of buttons on a stage.
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */
public class LevelSelectorScreen implements Screen {
	private final GravityMainGame g;
	private Skin skin;
	private Stage stage;
	private SpriteBatch batch;
	private final Table container;
	
	private static final int LEVELS_PER_ROW = 5;
	private static final int NUM_LEVELS = 10;
	private final String MSG_ERROR= "Level not Available";
	
	/**
	 * Constructs a level selection screen to let users choose their level
	 * @param g the GravityMainGame in which the LevelSelector will be constructed in
	 */
	public LevelSelectorScreen(final GravityMainGame g) {
		this.g = g;
		
		Skin skin = new Skin(Gdx.files.internal(Resources.images.UI_SKIN));
		
		batch = new SpriteBatch();
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		
		container = new Table();
		container.pad(50);
		
		stage.addActor(container);
		container.setFillParent(true);
		container.defaults().width(100).height(50);
		
		drawLevelButtons(NUM_LEVELS);
		
		container.row().height(50);
		TextButton toMenu = new TextButton("Main Menu", skin);
		toMenu.getStyle().up = (new TextureRegionDrawable(new TextureRegion(
				new Texture(Resources.images.SQUARE))));
		toMenu.center();
		toMenu.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				g.showMainMenu();
			}
		});
		
		container.add(toMenu).colspan(LEVELS_PER_ROW).expand();
	}


	/**
	 * Draw level selector's buttons in table
	 * @param numLevels the number of level buttons to be drawn
	 */
	public void drawLevelButtons(int numLevels) {
		for (int levelIndex = 0; levelIndex < numLevels; levelIndex++) {

			Skin skin = new Skin(Gdx.files.internal(Resources.images.UI_SKIN));
			Button btn = new TextButton((levelIndex + 1) + "", skin);

			if (Progress.isCompleted(levelIndex)) {
				// Draw levels that have been beaten in green
				btn.getStyle().up = (new TextureRegionDrawable(new TextureRegion(new Texture(Resources.images.SQUARE_CHECK))));
			} else {
				// Draw levels that haven't been beaten in red
				btn.getStyle().up = (new TextureRegionDrawable(new TextureRegion(new Texture(Resources.images.SQUARE))));
			}
			
			final int levelIndexFinal = levelIndex;

			btn.addListener(new ClickListener() {
				@Override
				public void clicked(InputEvent event, float x, float y) {
					try {
						g.playLevel(levelIndexFinal);
					} catch (JSONException e) {
						e.printStackTrace();
						g.showErrorMessage();
					} catch (GravityWellException e) {
						e.printStackTrace();
						g.showErrorMessage();
					} catch (IndexOutOfBoundsException e){
						e.printStackTrace();
						g.showErrorMessage(MSG_ERROR);
					}
				}
			});
			container.add(btn).expand();
			
			// New line after every 5 levels.
			if (levelIndex % LEVELS_PER_ROW == LEVELS_PER_ROW - 1)
				container.row().height(50);
		}
	}
	/**
	 * On each render iteration, draw the menu and ask all elements to act.
	 */
	@Override
	public void render(float time) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(Gdx.graphics.getDeltaTime());
		
		batch.begin();
		batch.draw(MainMenuScreen.BACKGROUND, 0, 0);
		batch.end();
		
		stage.draw();
		Table.drawDebug(stage);
	}

	/**
	 * System method; do nothing
	 */
	@Override
	public void resize(int width, int height) {
	}

	/**
	 * System method; do nothing
	 */
	@Override
	public void show() {
	}

	/**
	 * System method; do nothing
	 */
	@Override
	public void hide() {
	}

	/**
	 * System method; do nothing
	 */
	@Override
	public void pause() {
	}

	/**
	 * System method; do nothing
	 */
	@Override
	public void resume() {
	}

	/**
	 * Called manually to dispose of objects that might not otherwise be garbage
	 * collected.
	 */
	@Override
	public void dispose() {
		stage.dispose();
		batch.dispose();
	}
}
