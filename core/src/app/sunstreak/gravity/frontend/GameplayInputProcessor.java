package app.sunstreak.gravity.frontend;

import com.badlogic.gdx.InputProcessor;

/**
 * This input layer accepts touches and sends them to Grid for processing.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */
public class GameplayInputProcessor implements InputProcessor {

	LevelHandlerScreen master;

	/**
	 * Instantiate the input layer for gameplay.
	 * 
	 * @param master
	 *            the LevelHandler that uses this processor for input
	 */
	public GameplayInputProcessor(LevelHandlerScreen master) {
		this.master = master;
	}

	/**
	 * A keyboard key was pressed.
	 * 
	 * @param keycode
	 *            the key
	 * @return the keystroke was not processed
	 */
	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	/**
	 * A keyboard key was released.
	 * 
	 * @param keycode
	 *            the key
	 * @return the keystroke was not processed
	 */
	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	/**
	 * A keyboard keystroke was received.
	 * 
	 * @param character
	 *            the key
	 * @return the keystroke was not processed
	 */
	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	/**
	 * The screen was touched.
	 * 
	 * @param screenX
	 *            touch coordinates
	 * @param screenY
	 *            touch coordinates
	 * @param pointer
	 *            n/a
	 * @param button
	 *            for touches, always left
	 * @return the touch was processed
	 */
	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		master.startTouch();
		return true;
	}

	/**
	 * The screen was released.
	 * 
	 * @param screenX
	 *            touch coordinates
	 * @param screenY
	 *            touch coordinates
	 * @param pointer
	 *            n/a
	 * @param button
	 *            for touches, always left
	 * @return the release was processed
	 */
	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		master.stopTouch();
		return true;
	}

	/**
	 * The screen was touch-dragged.
	 * 
	 * @param screenX
	 *            touch coordinates
	 * @param screenY
	 *            touch coordinates
	 * @param pointer
	 *            n/a
	 * @return the touch-drag was not processed
	 */
	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	/**
	 * The mouse was moved.
	 * 
	 * @param screenX
	 *            mouse coordinates
	 * @param screenY
	 *            mouse coordinates
	 * @return the input was not processed
	 */
	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	/**
	 * The mouse was scrolled.
	 * 
	 * @param amount
	 *            scroll value
	 * @return the input was not processed
	 */
	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
