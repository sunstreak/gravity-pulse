package app.sunstreak.gravity.frontend;

import app.sunstreak.gravity.backend.SimplePhysics;
import app.sunstreak.gravity.util.Resources;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * The Compass can draw itself and rotates to point toward provided coordinates.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */
public class Compass {

	public final Texture COMPASS_TEXTURE;

	/**
	 * Instantiate a new Compass; load the Texture.
	 */
	public Compass() {
		COMPASS_TEXTURE = new Texture(Resources.images.ARROW);
	}

	/**
	 * Draw the Compass image rotated so that it points toward a specified
	 * global position.
	 * 
	 * @param sb
	 *            the SpriteBatch for drawing
	 * @param camera
	 *            the camera to relate screen to global coordinates
	 * @param targetPos
	 *            the global position to which the compass should point
	 */
	public void drawSelf(SpriteBatch sb, Camera camera, Vector2 targetPos) {
		int compassTextureWidth = COMPASS_TEXTURE.getWidth();

		int compassXPos = SimplePhysics.CAMERA_OFFSET + compassTextureWidth / 2;
		int compassYPos = SimplePhysics.SCRHEIGHT - compassTextureWidth / 2
				- SimplePhysics.CAMERA_OFFSET;
		Vector3 coords = new Vector3(compassXPos, compassYPos, 0);
		camera.unproject(coords);

		float deltaY = targetPos.y - coords.y;
		float deltaX = targetPos.x - coords.x;
		float angle = (float) Math.toDegrees(Math.atan(deltaY / deltaX));

		// Arc-tangent only returns angles in the range [-pi/2, pi/2].
		// If delta x < 0, the angle should be in the range [pi/2, 3pi/2].
		if (deltaX < 0)
			angle += 180;

		Sprite s = new Sprite(COMPASS_TEXTURE);
		s.setOrigin(compassTextureWidth / 2, COMPASS_TEXTURE.getHeight() / 2);
		s.setRotation(angle);
		s.setPosition(coords.x - compassTextureWidth / 2, coords.y
				- compassTextureWidth / 2);
		s.draw(sb);
	}
}
