package app.sunstreak.gravity.frontend;

import app.sunstreak.gravity.backend.SimplePhysics;
import app.sunstreak.gravity.util.Resources;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

/**
 * This class draws the pause button and processes input. If the input methods
 * indicate that the pause button was touched, pause signals are sent to the
 * game. If not, the input is not handled and is passed down by the multiplexer
 * in LevelHandler.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */
public class PauseButton implements InputProcessor {

	private final Texture PAUSE_BTN_TEX;
	private Vector3 screenCoords;
	private final int DIMENSION;
	private final int RADIUS;
	private final Screen PARENT_SCREEN;

	/**
	 * Instantiate a pause button. Sets the dimension variables for drawing and
	 * loads the texture.
	 * 
	 * @param parent
	 *            the parent screen that provides the on-pause methods
	 */
	public PauseButton(LevelHandlerScreen parent) {
		PAUSE_BTN_TEX = new Texture(Resources.images.PAUSE);
		DIMENSION = PAUSE_BTN_TEX.getWidth();
		RADIUS = DIMENSION / 2;
		PARENT_SCREEN = parent;
	}

	/**
	 * Fetch the texture of the pause button.
	 * 
	 * @return the pause texture
	 */
	public Texture getTexture() {
		return PAUSE_BTN_TEX;
	}

	/**
	 * Fetch the screen coordinates of the pause button's center.
	 * 
	 * @return the coordinates, from bottom-left
	 */
	public Vector3 getScreenCoords() {
		return screenCoords;
	}

	/**
	 * Instruct the pause button to draw itself on the screen.
	 * 
	 * @param batch
	 *            the SpriteBatch for drawing
	 * @param camera
	 *            the camera used to relate screen to global coordinates
	 */
	public void drawSelf(SpriteBatch batch, Camera camera) {
		int compassXPos = SimplePhysics.SCRWIDTH - RADIUS
				- SimplePhysics.CAMERA_OFFSET;
		int compassYPos = SimplePhysics.SCRHEIGHT - RADIUS
				- SimplePhysics.CAMERA_OFFSET;
		Vector3 coords = new Vector3(compassXPos, compassYPos, 0);
		screenCoords = coords.cpy();
		camera.unproject(coords);

		Sprite s = new Sprite(PAUSE_BTN_TEX);

		s.setPosition(coords.x - RADIUS, coords.y - RADIUS);
		s.draw(batch);
	}

	/**
	 * A keyboard key was pressed.
	 * 
	 * @param keycode
	 *            the key
	 * @return the keystroke was not processed
	 */
	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	/**
	 * A keyboard key was released.
	 * 
	 * @param keycode
	 *            the key
	 * @return the keystroke was not processed
	 */
	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	/**
	 * A keyboard keystroke was received.
	 * 
	 * @param character
	 *            the key
	 * @return the keystroke was not processed
	 */
	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	/**
	 * The screen was touched. If the touch is closer to the center of the pause
	 * button than the radius, return true to indicate that the input was
	 * processed and pause the game. If not, return false.
	 * 
	 * @param screenX
	 *            touch coordinates
	 * @param screenY
	 *            touch coordinates
	 * @param pointer
	 *            n/a
	 * @param button
	 *            for touches, always left
	 * @return whether the touch was processed
	 */
	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// if distance between pause button center and touch is less than button
		// radius, button was clicked
		if (new Vector3(screenX, screenY, 0).dst2(screenCoords) < RADIUS
				* RADIUS) {
			if (((LevelHandlerScreen) PARENT_SCREEN).isPaused())
				return false;
			PARENT_SCREEN.pause();
			return true;
		}
		return false;
	}

	/**
	 * The screen was released.
	 * 
	 * @param screenX
	 *            touch coordinates
	 * @param screenY
	 *            touch coordinates
	 * @param pointer
	 *            n/a
	 * @param button
	 *            for touches, always left
	 * @return the release was not processed
	 */
	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	/**
	 * The screen was touch-dragged.
	 * 
	 * @param screenX
	 *            touch coordinates
	 * @param screenY
	 *            touch coordinates
	 * @param pointer
	 *            n/a
	 * @return the touch-drag was not processed
	 */
	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	/**
	 * The mouse was moved.
	 * 
	 * @param screenX
	 *            mouse coordinates
	 * @param screenY
	 *            mouse coordinates
	 * @return the input was not processed
	 */
	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	/**
	 * The mouse was scrolled.
	 * 
	 * @param amount
	 *            scroll value
	 * @return the input was not processed
	 */
	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
