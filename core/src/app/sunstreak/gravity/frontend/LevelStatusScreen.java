package app.sunstreak.gravity.frontend;

import org.json.JSONException;

import app.sunstreak.gravity.backend.GravityMainGame;
import app.sunstreak.gravity.util.LevelFormatException;
import app.sunstreak.gravity.util.Resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
/**
 * This class is the Screen that allows the user to see whether they succeeded or lost the
 * level they just played. It also allows for a replay of that level or a link to level selection
 * screen for other playable levels.
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */
public class LevelStatusScreen implements Screen {
	private final GravityMainGame g;
	private final Table container;
	private final SpriteBatch batch;
	private final Stage stage;
	private Skin skin;
	
	private static final String SELECT_ANOTHER_LEVEL = "Select another level";
	private static final String REPLAY_LEVEL = "Replay level";

	/**
	 * /**
	 * Instantiate the screen that renders the status of a certain level after completion
	 * or failure
	 * @param game the GravityMainGame in which the LevelStatus will be drawn
	 * @param message message to display
	 * @param showReplayButton whether to show the replay button
	 */
	public LevelStatusScreen(GravityMainGame game, String message,
			boolean showReplayButton) {
		this.g = game;
		
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		
		batch = new SpriteBatch();
		
		skin = new Skin(Gdx.files.internal(Resources.images.UI_SKIN));
		
		container = new Table();
		container.pad(50);
		stage.addActor(container);
		container.setFillParent(true);
		container.defaults().width(100).height(50);
		drawItems(message, showReplayButton);
	}
	
	/**
	 * Draw the items on the screen for user interaction
	 * @param message to display
	 * @param showReplayButton whether to show the replay button 
	 */
	public void drawItems (String message, boolean showReplayButton) {
		Label text = new Label(message, skin);
		text.setAlignment(Align.center);
		container.add(text).colspan(2);
		container.row();
		Button selectAnotherLevelButton = new TextButton(SELECT_ANOTHER_LEVEL, skin);
		
		selectAnotherLevelButton.getStyle().up = (new TextureRegionDrawable(new TextureRegion(new Texture(Resources.images.SQUARE))));
		
		selectAnotherLevelButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				g.openLevelSelector();
			}
		});
		
		container.add(selectAnotherLevelButton).width(200);
		
		if (showReplayButton) {
			
			Button replayButton = new TextButton(REPLAY_LEVEL, skin);
			
			replayButton.getStyle().up = (new TextureRegionDrawable(new TextureRegion(new Texture(Resources.images.SQUARE))));
			
			replayButton.addListener(new ClickListener() {
				@Override
				public void clicked(InputEvent event, float x, float y) {
					try {
						g.replayLevel();
					} catch (LevelFormatException e) {
						e.printStackTrace();
						g.showErrorMessage();
					} catch (JSONException e) {
						e.printStackTrace();
						g.showErrorMessage();
					}
				}
			});
			
			container.add(replayButton).width(200);
		}
			
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glClearColor(0, 0, 0, 0);
		stage.act(Gdx.graphics.getDeltaTime());
		
		batch.begin();
		batch.draw(MainMenuScreen.BACKGROUND, 0, 0);
		batch.end();
		
		stage.draw();
		Table.drawDebug(stage);
	}

	/**
	 * System method; do nothing
	 */
	@Override
	public void resize(int width, int height) {
	}

	/**
	 * System method; do nothing
	 */
	@Override
	public void show() {
	}

	/**
	 * System method; do nothing
	 */
	@Override
	public void hide() {
	}

	/**
	 * System method; do nothing
	 */
	@Override
	public void pause() {
	}

	/**
	 * System method; do nothing
	 */
	@Override
	public void resume() {
	}

	/**
	 * Called manually to dispose of objects that might not otherwise be garbage
	 * collected.
	 */
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		stage.dispose();
		batch.dispose();
	}
}
