package app.sunstreak.gravity.backend;

import java.util.ArrayList;
import java.util.Collection;

import app.sunstreak.gravity.actor.InvisibleGravitator;
import app.sunstreak.gravity.actor.Repulsor;
import app.sunstreak.gravity.actor.SpaceActor;

import com.badlogic.gdx.math.Vector2;

/**
 * The Grid manages the SpaceActors. It performs physics on each Actor relative
 * to the other Actors, and it updates the data of all the Actors.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */

public class Grid {

	private final ArrayList<SpaceActor> spaceActors;
	// The special SpaceActor that provides user input
	private final InvisibleGravitator finger;

	private final LevelStatusManager levelStatusManager;

	// the dimensions of the Grid
	public final int WIDTH;
	public final int HEIGHT;

	// Neglect gravity for objects further apart than this distance
	public final int GRAVITY_NEGLECT_DISTANCE = 1600;
	public final int GRAVITY_NEGLECT_DISTANCE_SQUARED = GRAVITY_NEGLECT_DISTANCE
			* GRAVITY_NEGLECT_DISTANCE;

	/**
	 * Construct a grid of given dimensions.
	 * 
	 * @param width
	 *            The grid width
	 * @param height
	 *            The grid height
	 */
	public Grid(Level level) {
		Vector2 gridSize = level.getGridSize();
		WIDTH = (int) gridSize.x;
		HEIGHT = (int) gridSize.y;

		finger = new InvisibleGravitator();
		spaceActors = level.getSpaceActors();
		spaceActors.add(finger);

		levelStatusManager = new LevelStatusManager(level.getShips());
	}

	/**
	 * Fetch a Collection of SpaceActors managed by this Grid.
	 * 
	 * @return the Collection of SpaceActors
	 */
	public Collection<SpaceActor> getSpaceActors() {
		return spaceActors;
	}

	/**
	 * Perform physics on all SpaceActors, thus advancing a "frame."
	 * 
	 * @param time
	 *            the time elapsed since tick() was last called.
	 */
	public void tick(float time) {
		updateVelocities(time);
		checkForCollisions(time);
		updatePositions(time);
	}

	/**
	 * Called when the input layer detects the screen has been touched.
	 * 
	 * @param touchPos
	 *            the coordinates of the touch
	 */
	public void touch(Vector2 touchPos) {
		// A simple hack to (most of the time) prevent the ship from going
		// outside the bounds
		if (SimplePhysics.isBetween(25, WIDTH - 25, touchPos.x)
				&& SimplePhysics.isBetween(25, HEIGHT - 25, touchPos.y))
			finger.on(touchPos);
	}

	/**
	 * Called when the input layer detects a touch has ended.
	 */
	public void stopTouch() {
		finger.off();
	}

	/**
	 * Update the velocities of all the SpaceActors in the Grid, using the
	 * forces they exert on each other.
	 * 
	 * @param time
	 *            the time elapsed since the last physics computation loop
	 */
	private void updateVelocities(float time) {
		// This array will hold all of the forces
		Vector2[] forces = new Vector2[spaceActors.size()];
		for (int i = 0; i < forces.length; i++)
			forces[i] = new Vector2();
		// for all possible pairings of SpaceActors
		for (int i = 0; i < spaceActors.size(); i++) {
			for (int j = i + 1; j < spaceActors.size(); j++) {
				SpaceActor a = spaceActors.get(i);
				SpaceActor b = spaceActors.get(j);
				// Neglect gravity between faraway objects for efficiency
				if (a.getPosition().dst2(b.getPosition()) < GRAVITY_NEGLECT_DISTANCE_SQUARED) {
					boolean aMoves = a.isMovable() && b.hasGravity();
					boolean bMoves = b.isMovable() && a.hasGravity();
					// Check if finger is too close to planet
					if (a == finger) {
						if (bMoves && ((InvisibleGravitator) a).tooCloseTo(b)) {
							bMoves = false;
						}
					} else if (b == finger) {
						if (aMoves && ((InvisibleGravitator) b).tooCloseTo(a)) {
							aMoves = false;
						}
					}
					// if either SpaceActor can move, update the forces exerted
					Vector2 force = SimplePhysics.getGravForce(a, b);
					if (aMoves) {
						if (!(b instanceof Repulsor)) // repulsor has neg. mass
							forces[i].sub(force);
						else
							forces[i].add(force);
					}
					if (bMoves) {
						if (!(a instanceof Repulsor))
							forces[j].add(force);
						else
							forces[j].sub(force);
					}
				}
			}
		}
		// apply the newly calculated forces to change the velocities
		for (int i = 0; i < spaceActors.size(); i++) {
			spaceActors.get(i).applyForce(forces[i], time);
		}
	}

	/**
	 * Update the positions of all SpaceActors. The Actors already know their
	 * own updated velocities.
	 * 
	 * @param time
	 *            the time elapsed since the last physics computation loop
	 */
	private void updatePositions(float time) {
		for (SpaceActor s : spaceActors) {
			if (s.isMovable()) {
				s.move(time);
			}
		}
	}

	/**
	 * Parse through all possible SpaceActor pairings and instruct one of the
	 * Actors involved in each potential collision to check for that collision.
	 * 
	 * @param time
	 *            the time elapsed since the last physics computation loop
	 */
	private void checkForCollisions(float time) {
		SpaceActor a, b;
		for (int i = 0; i < spaceActors.size(); i++) {
			for (int j = i + 1; j < spaceActors.size(); j++) {
				a = spaceActors.get(i);
				b = spaceActors.get(j);
				a.collide(b, time);
			}
		}
	}

	/**
	 * Get the elapsed time for this level.
	 * 
	 * @return the elapsed time
	 */
	public double getGameTimeElapsed() {
		return levelStatusManager.getElapsedTime();
	}

	/**
	 * Called when a parent class is disposed. Dispose of all the SpaceActors.
	 */
	public void dispose() {
		for (SpaceActor s : spaceActors)
			s.dispose();
	}
}
