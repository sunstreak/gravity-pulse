package app.sunstreak.gravity.backend;

import java.util.ArrayList;
import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.sunstreak.gravity.actor.Debris;
import app.sunstreak.gravity.actor.Gate;
import app.sunstreak.gravity.actor.Medicine;
import app.sunstreak.gravity.actor.Planet;
import app.sunstreak.gravity.actor.Repulsor;
import app.sunstreak.gravity.actor.Ship;
import app.sunstreak.gravity.actor.SpaceActor;
import app.sunstreak.gravity.actor.Wall;
import app.sunstreak.gravity.actor.Wormhole;
import app.sunstreak.gravity.util.LevelFormatException;
import app.sunstreak.gravity.util.Resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;

/**
 * This class handles the creation of a specific level through an input JSON
 * file. It generates the SpaceActors, and it delegates the rendering and
 * physics.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kapur
 * @author Han Li
 */
public class Level {

	private final JSONObject obj;
	private final ArrayList<SpaceActor> spaceActors;
	private final Vector2 gridSize;
	// for single-player games, this should only contain one ship
	private final ArrayList<Ship> ships;

	/**
	 * Construct level with file
	 * 
	 * @param gameData
	 *            the file to create level with
	 * @throws LevelFormatException 
	 */
	public Level(FileHandle gameData) throws JSONException, LevelFormatException {
		// Read in the level data from JSON file
		obj = new JSONObject(gameData.readString());

		gridSize = parseGridSize(obj);

		// Generate Collection<SpaceActor> of actors in this level
		spaceActors = parseSpaceActors(obj);
		ships = filterShipsFromSpaceActors(spaceActors);
	}

	/**
	 * Gets the collections of SpaceActors instantiated as specified in the
	 * input file.
	 * 
	 * @return collection of SpaceActors
	 */
	public ArrayList<SpaceActor> getSpaceActors() {
		return spaceActors;
	}

	public ArrayList<Ship> getShips() {
		return ships;
	}

	/**
	 * Get the dimensions of the grid as specified in the file.
	 * 
	 * @return Vector2 of grid size
	 */
	public Vector2 getGridSize() {
		return gridSize;
	}

	/**
	 * Using the JSONObject created from the file, creates specified
	 * SpaceActors.
	 * 
	 * @param obj
	 *            JSONObject created by file reading
	 * @return ArrayList of SpaceActors
	 * @throws JSONException
	 * @throws LevelFormatException 
	 */
	private static ArrayList<SpaceActor> parseSpaceActors(JSONObject obj)
			throws JSONException, LevelFormatException {
		ArrayList<SpaceActor> ret = new ArrayList<SpaceActor>();
		JSONArray spaceActors = obj.getJSONArray("space actors");
		for (int i = 0; i < spaceActors.length(); i++) {
			JSONObject actor = spaceActors.getJSONObject(i);
			String type = actor.getString("type");
			if (type.equals("wormhole pair")) {
				ret.addAll(createWormholePair(actor));
			} else {
				ret.add(createSpaceActor(actor));
			}
		}
		// get grid data
		JSONObject grid = obj.getJSONObject("grid");
		if (grid.getBoolean("bounded")) {
			ret.addAll(createBoundedWalls(parseGridSize(obj)));
		}
		return ret;
	}

	/**
	 * Returns a Collection of all Ships that are in a list of SpaceActors.
	 * 
	 * @param spaceActors
	 *            a Collection of SpaceActors
	 * @returna Collection of all Ships that are in a list of SpaceActors.
	 */
	private static ArrayList<Ship> filterShipsFromSpaceActors(
			Collection<SpaceActor> spaceActors) {
		ArrayList<Ship> ships = new ArrayList<Ship>(1);

		for (SpaceActor actor : spaceActors) {
			if (actor instanceof Ship) {
				ships.add((Ship) actor);
			}
		}
		return ships;
	}

	/**
	 * Get the grid dimensions from the JSONObject.
	 * 
	 * @param obj
	 *            JSONObject to parse
	 * @return Vector2 of grid size
	 * @throws JSONException
	 */
	private static Vector2 parseGridSize(JSONObject obj) throws JSONException {
		JSONObject grid = obj.getJSONObject("grid");
		return parseVector2(grid.getJSONObject("size"));
	}

	/**
	 * Create bounding walls for the level based on the grid dimensions.
	 * 
	 * @param gridSize
	 *            size of the wall to be created
	 * @return Collection of wall objects to be added
	 */
	private static Collection<Wall> createBoundedWalls(Vector2 gridSize) {
		// add walls on all sides
		ArrayList<Wall> walls = new ArrayList<Wall>(4);

		final float gridWidth = gridSize.x;
		final float gridHeight = gridSize.y;

		walls.add(new Wall(new Vector2(0, 0 - Wall.BOUNDING_WIDTH / 2),
				gridWidth, Wall.BOUNDING_WIDTH));
		walls.add(new Wall(new Vector2(0 - Wall.BOUNDING_WIDTH / 2, 0),
				Wall.BOUNDING_WIDTH, gridHeight));
		walls.add(new Wall(
				new Vector2(0, gridHeight - Wall.BOUNDING_WIDTH / 2),
				gridWidth, Wall.BOUNDING_WIDTH));
		walls.add(new Wall(new Vector2(gridWidth - Wall.BOUNDING_WIDTH / 2, 0),
				Wall.BOUNDING_WIDTH, gridHeight));
		return walls;
	}

	/**
	 * Create wormholes from JSONObject input.
	 * 
	 * @param obj
	 *            JSONObject to parse
	 * @return Collection of wormholes
	 * @throws JSONException
	 */
	private static Collection<Wormhole> createWormholePair(JSONObject obj)
			throws JSONException {

		int mass = obj.getInt("mass");

		Vector2 sourcePosition = parseVector2(obj.getJSONObject("source")
				.getJSONObject("position"));
		Vector2 destinationPosition = parseVector2(obj.getJSONObject(
				"destination").getJSONObject("position"));
		return Wormhole.createWormholePair(sourcePosition, destinationPosition,
				mass, 0);
	}

	/**
	 * Create a SpaceActor from JSONObject input.
	 * 
	 * @param actor
	 *            JSONObject to parse
	 * @return SpaceActor created from parsing JSONObject provided
	 * @throws LevelFormatException 
	 * @throws JSONException
	 */
	private static SpaceActor createSpaceActor(JSONObject actor)
			throws JSONException, LevelFormatException {
		String type = actor.getString("type");

		Vector2 position = parseVector2(actor.getJSONObject("position"));

		if (type.equals("wall")) {
			int wallWidth = actor.getInt("width");
			int wallHeight = actor.getInt("height");
			return new Wall(position, wallWidth, wallHeight);
		} else {
			Vector2 velocity = parseVector2(actor.getJSONObject("velocity"));
			int mass = actor.getInt("mass");
			
			if (actor.has("img")) {
				/*
				 * JSON has custom defined images for actor
				 */
				
				String imgFileHandlePath = Resources.images.PREFIX + actor.getString("img");
				FileHandle imgFileHandle = Gdx.files.internal(imgFileHandlePath);
				
				if (!imgFileHandle.exists())
					throw new RuntimeException("Image file \"" + imgFileHandlePath + "\" not found.");
				
				try {
					return createSpaceActorWithImage(type, position, velocity, mass, imgFileHandle);
				} catch (LevelFormatException e) {
					return createSpaceActorWithoutImage(type, position, velocity, mass);
				}
				
			} else {
				return createSpaceActorWithoutImage(type, position, velocity, mass);
			}
		}
	}
	
	public static SpaceActor createSpaceActorWithImage(String type, Vector2 position, Vector2 velocity, int mass, FileHandle imgFileHandle) throws LevelFormatException {
		if (type.equals(Gate.JSON_NAME))
			return new Gate(position, velocity, mass, imgFileHandle);
		else if (type.equals(Debris.JSON_NAME))
			return new Debris(position, velocity, mass, imgFileHandle);
		else if (type.equals(Planet.JSON_NAME))
			return new Planet(position, velocity, mass, imgFileHandle);
		else if (type.equals(Repulsor.JSON_NAME))
			return new Repulsor(position, velocity, mass, imgFileHandle);
		else
			throw new LevelFormatException("Either \"" + type + "\" is not a valid actor, or it does not support custom image files.");
	}
	
	public static SpaceActor createSpaceActorWithoutImage(String type, Vector2 position, Vector2 velocity, int mass) throws LevelFormatException {
		if (type.equals(Ship.JSON_NAME))
			return new Ship(position, velocity, mass);
		else if (type.equals(Gate.JSON_NAME))
			return new Gate(position, velocity, mass);
		else if (type.equals(Debris.JSON_NAME))
			return new Debris(position, velocity, mass);
		else if (type.equals(Planet.JSON_NAME))
			return new Planet(position, velocity, mass);
		else if (type.equals(Repulsor.JSON_NAME))
			return new Repulsor(position, velocity, mass);
		else if (type.equals(Medicine.JSON_NAME))
			return new Medicine(position, velocity, mass);
		else
			throw new LevelFormatException("Actor not supported: " + type);
	}

	/**
	 * Parse an arbitrary Vector2 from a JSONObject.
	 * 
	 * @param obj
	 *            JSONObject to parse
	 * @return Vector2 parsed from JSONObject
	 * @throws JSONException
	 */
	private static Vector2 parseVector2(JSONObject obj) throws JSONException {
		float x = (float) obj.getDouble("x");
		float y = (float) obj.getDouble("y");
		return new Vector2(x, y);
	}
}
