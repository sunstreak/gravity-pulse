package app.sunstreak.gravity.backend;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.sunstreak.gravity.util.LevelFormatException;
import app.sunstreak.gravity.util.Resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

/**
 * This class manages the level files to be created using data from assets.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kapur
 * @author Han Li
 */
public class Levels {

	private Levels() {
		/* Don't instantiate me */
	}

	private static List<String> levelFileNames;

	/**
	 * This static initializer block performs boilerplate "loading" logic.
	 */
	static {
		FileHandle fh = Gdx.files.internal(Resources.LEVELS_FILE_NAME);
		JSONArray arr = null;
		try {
			arr = new JSONObject(fh.readString()).getJSONArray("levels");
			levelFileNames = new ArrayList<String>(arr.length());

			for (int i = 0; i < arr.length(); i++)
				levelFileNames.add(arr.getString(i));
		} catch (JSONException e) {
			// the exception is silent
		}
	}

	/**
	 * Instantiate the level given the 0-based index of the level.
	 * 
	 * @param index
	 *            the place to get the file
	 * @return Level created from that file
	 * @throws JSONException
	 * @throws LevelFormatException 
	 */
	public static Level createLevel(int index) throws JSONException, LevelFormatException, IndexOutOfBoundsException {
		return new Level(Gdx.files.internal(Resources.LEVELS_PREFIX
				+ levelFileNames.get(index)));
	}

	/**
	 * Get total number of levels.
	 * 
	 * @return int number of levels
	 */
	public static int getNumLevels() {
		return levelFileNames.size();
	}
}
