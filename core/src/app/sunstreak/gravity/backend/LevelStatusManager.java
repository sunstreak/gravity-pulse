package app.sunstreak.gravity.backend;

import java.util.Collection;

import app.sunstreak.gravity.actor.Ship;

/**
 * The LevelStatusManager provides architecture to manage Level state as well as
 * Ship collision tracking. TODO: Ship collision and game state management not
 * yet refactored into this class.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */

public class LevelStatusManager {

	// contains all Ships; for single-player mode, should only contain one
	private final Collection<Ship> ships;
	private final long start;

	/**
	 * The gameplay status of the active Level
	 */
	public enum LevelStatus {
		IN_PROGRESS, WIN, LOSS;
	}

	/**
	 * Instantiate a LevelStatusManager.
	 * 
	 * @param ships
	 *            all ships to track
	 */
	public LevelStatusManager(Collection<Ship> ships) {
		this.ships = ships;
		start = System.nanoTime();
	}

	/**
	 * Get the elapsed time for this level, in seconds.
	 * 
	 * @return the elapsed time, in seconds
	 */
	public double getElapsedTime() {
		return (System.nanoTime() - start) / 1000000000.0;
	}
}
