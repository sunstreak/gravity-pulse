package app.sunstreak.gravity.backend;

import app.sunstreak.gravity.actor.SpaceActor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

/**
 * This class provides a static library of methods to perform essential physics
 * and math computations.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */
public final class SimplePhysics {

	// These are used for the camera viewport as well as texture scaling.
	// Using constants instead of the actual screen width will fix the viewport
	// and scaling at particular values.
	public static final int SCRWIDTH = Gdx.graphics.getWidth();
	public static final int SCRHEIGHT = Gdx.graphics.getHeight();
	public static final int CAMERA_OFFSET = 25;

	// The gravitational constant is arbitrary
	private static final double GRAVITATIONAL_CONSTANT = 2E-3;

	/**
	 * The private constructor prevents instantiations; this is a utilities
	 * class.
	 */
	private SimplePhysics() {
		// Instantiation disallowed
	}

	/**
	 * Get the magnitude of the force between two bodies.
	 * 
	 * @param distanceSquared
	 *            the square of the distance between the bodies
	 * @param m1
	 *            the mass of the first body
	 * @param m2
	 *            the mass of the second body
	 * @return the gravitational force between the bodies
	 */
	public static float oneDimensionalGravity(float distanceSquared, double m1,
			double m2) {
		return (float) (GRAVITATIONAL_CONSTANT * m1 * m2 / distanceSquared);
	}

	/**
	 * Determine whether a real number is between two other real numbers,
	 * inclusive.
	 * 
	 * @param left
	 *            the lower boundary
	 * @param right
	 *            the upper boundary
	 * @param val
	 *            the value to check
	 * @return whether the value is in the provided range.
	 */
	public static boolean isBetween(float left, float right, float val) {
		return val >= left && val <= right;
	}

	/**
	 * Find the gravitational force in Newtons that SpaceActor a exerts on
	 * SpaceActor b
	 * 
	 * @param a
	 *            the SpaceActor that exerts force
	 * @param b
	 *            the SpaceActor the receives the force
	 * @return the 2d force vector
	 */
	public static Vector2 getGravForce(SpaceActor a, SpaceActor b) {
		Vector2 force = new Vector2();

		if (a.getMass() > 0 && b.getMass() > 0) {
			float distanceSquared = a.getPosition().dst2(b.getPosition());
			float forceMagnitude = oneDimensionalGravity(distanceSquared,
					a.getMass(), b.getMass());

			Vector2 deltaPos = a.getPosition().cpy().sub(b.getPosition());
			force = deltaPos.limit(1).scl(forceMagnitude);
		}

		return force;
	}

	/**
	 * Clamps the value f in the range [min, max]. If f is within range, returns
	 * f. If f is below range, returns min. If f is above range, returns max.
	 * 
	 * @param f
	 *            the value to clamp
	 * @param min
	 *            the min value
	 * @param max
	 *            the max value
	 * @return the clamped value
	 */
	public static float clamp(float f, float min, float max) {
		return Math.max(min, Math.min(max, f));
	}

	/**
	 * Clamps the value f in the range [min, max]. If f is within range, returns
	 * f. If f is below range, returns min. If f is above range, returns max.
	 * 
	 * @param f
	 *            the value to clamp
	 * @param min
	 *            the min value
	 * @param max
	 *            the max value
	 * @return the clamped value
	 */
	public static int clamp(int f, int min, int max) {
		return Math.max(min, Math.min(max, f));
	}
}
