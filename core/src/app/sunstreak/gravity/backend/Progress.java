package app.sunstreak.gravity.backend;

import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import app.sunstreak.gravity.util.Resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

/**
 * This class manages, saves, and reads the progress of the user through the
 * game levels.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kapur
 * @author Han Li
 */
public final class Progress {
	private static final HashMap<Integer, Integer> progressMap;
	private static final FileHandle fh;

	private Progress() {
		// instantiation disallowed
	}

	/**
	 * Static initializer loads the Map from the JSON.
	 */
	static {
		fh = Gdx.files.local(Resources.PROGRESS_FILE_NAME);
		progressMap = readProgressJSON();
	}

	/**
	 * Called upon completion of a level; sets the level as completed and saves
	 * a high score. Level Indices are 0-based.
	 * 
	 * @param levelIndex
	 *            the level completed
	 * @param score
	 *            the score when completing the level
	 */
	public static void setCompleted(int levelIndex, int score) {
		// The player did not beat the high score
		if (isCompleted(levelIndex) && progressMap.get(levelIndex) > score)
			return;

		progressMap.put(levelIndex, score);
		saveProgressToFile();
	}

	/**
	 * Check if the level has already been completed
	 * 
	 * @param levelIndex
	 *            level to check if completed
	 * @return boolean completed or not
	 */
	public static boolean isCompleted(int levelIndex) {
		return progressMap.containsKey(levelIndex);
	}

	/**
	 * Get the high score for a level.
	 * 
	 * @param levelIndex
	 *            level to get high score of
	 * @return int high score of level
	 */
	public static int getHighScore(int levelIndex) {
		if (!isCompleted(levelIndex))
			return 0;
		return progressMap.get(levelIndex);
	}

	/**
	 * Get the total score for all levels.
	 * 
	 * @return int total score
	 */
	public static int getTotalScore() {
		int total = 0;
		for (int i = 0; i < Levels.getNumLevels(); i++)
			total += getHighScore(i);
		return total;
	}

	/**
	 * Get the high score of all levels.
	 * 
	 * @return String[] of each level's high score
	 */
	public static String[] getHighScores() {
		String[] highScores = new String[Levels.getNumLevels()];
		for (int i = 0; i < Levels.getNumLevels(); i++)
			highScores[i] = "Level " + (i + 1) + ": " + getHighScore(i + 1);
		return highScores;
	}

	/**
	 * Read the entries for each level and its associated high score into a
	 * HashMap.
	 * 
	 * @return HashMap of previous progress
	 */
	private static HashMap<Integer, Integer> readProgressJSON() {
		if (!Gdx.files.isLocalStorageAvailable()) {
			return new HashMap<Integer, Integer>();
		}
		HashMap<Integer, Integer> data = new HashMap<Integer, Integer>();
		try {

			if (!fh.exists()) {
				fh.writeString(new JSONObject().toString(), false);
			}

			JSONObject in = new JSONObject(fh.readString());
			Iterator<?> i = in.keys();

			while (i.hasNext()) {
				String key = (String) i.next();
				Integer value = in.getInt(key);
				data.put(Integer.parseInt(key), value);
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return data;
	}

	/**
	 * Write to the disk the data stored in progress HashMap.
	 */
	private static void saveProgressToFile() {

		if (Gdx.files.isLocalStorageAvailable()) {

			try {
				JSONObject jo = new JSONObject();

				for (Integer i : progressMap.keySet()) {
					jo.put(i.toString(), progressMap.get(i).toString());
				}

				fh.writeString(jo.toString(), false);

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
}
