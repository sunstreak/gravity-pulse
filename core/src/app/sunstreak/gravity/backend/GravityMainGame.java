package app.sunstreak.gravity.backend;

import org.json.JSONException;

import app.sunstreak.gravity.frontend.HighScoreScreen;
import app.sunstreak.gravity.frontend.LevelHandlerScreen;
import app.sunstreak.gravity.frontend.LevelSelectorScreen;
import app.sunstreak.gravity.frontend.LevelStatusScreen;
import app.sunstreak.gravity.frontend.MainMenuScreen;
import app.sunstreak.gravity.util.LevelFormatException;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;

/**
 * This class manages the Game architecture, and handles the relationships
 * between screens. It provides an architecture for screen switching beyond
 * setScreen() in libgdx. It is the main libgdx glass for the game.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */

public class GravityMainGame extends Game {

	// this is the most recent game level index played; this is 0-based
	private int currentLevelIndex;

	/**
	 * Called automatically; loads the main menu screen.
	 */
	@Override
	public void create() {
		setScreenAndDispose(new MainMenuScreen(this));
	}

	/**
	 * Given a level index, load the level and set the appropriate screen. Level
	 * indices are 0-based.
	 * 
	 * @param levelNum
	 *            the level to play
	 * @throws JSONException
	 *             this exception is thrown if the Level's JSON data file fails
	 *             to parse
	 * @throws LevelFormatException 
	 */
	public void playLevel(int levelIndex) throws JSONException, LevelFormatException {
		setScreenAndDispose(new LevelHandlerScreen(
				Levels.createLevel(levelIndex), this));
		currentLevelIndex = levelIndex;
	}

	/**
	 * Replay the previously played level
	 * 
	 * @throws JSONException
	 *             this exception is thrown if the Level's JSON data file fails
	 *             to parse
	 * @throws LevelFormatException 
	 */
	public void replayLevel() throws JSONException, LevelFormatException {
		playLevel(currentLevelIndex);
	}

	/**
	 * This generates an error screen if the requested level number is unknown.
	 */
	public void showErrorMessage() {
		showErrorMessage("Level Not Available");
	}

	/**
	 * This allows the error screen message to be specified.
	 * 
	 * @param message
	 *            the error message
	 */
	public void showErrorMessage(String message) {
		setScreenAndDispose(new LevelStatusScreen(this, message, false));
	}

	/**
	 * Set the screen to the Level Selector.
	 */
	public void openLevelSelector() {
		setScreenAndDispose(new LevelSelectorScreen(this));
	}

	/**
	 * Set the screen to the High Score screen.
	 */
	public void showHighScoreScreen() {
		setScreenAndDispose(new HighScoreScreen(this));
	}

	/**
	 * Set the screen to the Main Menu.
	 */
	public void showMainMenu() {
		setScreenAndDispose(new MainMenuScreen(this));
	}

	/**
	 * This method is called when the currentLevel is won. The Progress class
	 * updates the game progress file, and the screen is changed.
	 * 
	 * @param livesRemaining
	 *            the number of shields the ship has upon winning
	 * @param the
	 *            time elapsed, in seconds, during the level
	 */
	public void win(int livesRemaining, double timeElapsed) {
		int score = (int) Math.max(livesRemaining * 100, livesRemaining * 1000
				- timeElapsed);
		Progress.setCompleted(currentLevelIndex, score);
		setScreenAndDispose(new LevelStatusScreen(this,
				"Level Complete. Score: " + score + " points.", true));
	}

	/**
	 * This method is called when the currentLevel is lost. Changes the screen.
	 */
	public void lose() {
		setScreenAndDispose(new LevelStatusScreen(this, "Level Failed.", true));
	}

	/**
	 * Update the screen, disposing of resources belonging to the previous
	 * screen.
	 * 
	 * @param screen
	 *            the new screen to display
	 */
	private void setScreenAndDispose(Screen screen) {
		Screen oldScreen = getScreen();
		super.setScreen(screen);

		if (oldScreen != null)
			oldScreen.dispose();
	}

	/**
	 * Client code is not allowed to change the screen directly. Instead, it
	 * must use the framework provided by Game.
	 * 
	 * @param s
	 *            a Screen; does nothing
	 */
	@Override
	public void setScreen(Screen s) {
		// Do nothing
	}
}
