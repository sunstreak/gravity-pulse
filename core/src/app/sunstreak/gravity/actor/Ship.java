package app.sunstreak.gravity.actor;

import app.sunstreak.gravity.actor.collide.CircleCollisionDetector;
import app.sunstreak.gravity.actor.collide.CollisionDetector;
import app.sunstreak.gravity.backend.LevelStatusManager;
import app.sunstreak.gravity.backend.SimplePhysics;
import app.sunstreak.gravity.util.Resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

/**
 * A Ship is a special SpaceActor that is movable and does not have gravity. It
 * behaves specially upon collisions with the various other kinds of special
 * SpaceActors, and it knows when to initiate the end of a game as per its
 * remaining health and collision status.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */
public class Ship extends SpaceActor {

	// The proportion of the screen's width that the texture should be scaled to
	// occupy
	public float WIDTH_PROPORTION = .1f;
	public static final String JSON_NAME = "ship";

	private final int INITIAL_HEALTH_LEVEL = 3;

	private int healthLevel;

	private LevelStatusManager.LevelStatus status;

	private Texture texture;
	private final float mass;

	/**
	 * Construct an instance of a Ship.
	 * 
	 * @param startPos
	 *            the starting coordinates, from bottom-left
	 * @param startVel
	 *            the starting velocity, in x/y components
	 * @param mass
	 *            the mass
	 */
	public Ship(Vector2 startPos, Vector2 startVel, int mass) {
		texture = new Texture(Gdx.files.internal(Resources.images.SHIP[3]));
		this.mass = mass;
		initialize(startPos, startVel);
		healthLevel = INITIAL_HEALTH_LEVEL;
		status = LevelStatusManager.LevelStatus.IN_PROGRESS;
	}

	/**
	 * Fetch the number of lives the ship has remaining.
	 * 
	 * @return the number of lives (shields).
	 */
	public int getHealthLevel() {
		return healthLevel;
	}

	/**
	 * {@inheritDoc} A ship is movable.
	 */
	@Override
	public boolean isMovable() {
		return true;
	}

	/**
	 * {@inheritDoc} A ship does not have gravity.
	 */
	@Override
	public boolean hasGravity() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float getMass() {
		return mass;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float proportionScreenWidth() {
		return WIDTH_PROPORTION;
	}

	/**
	 * {@inheritDoc} A ship has a circular collision detector.
	 */
	@Override
	public CollisionDetector createCollisionDetector() {
		return new CircleCollisionDetector(getPosition(), textureHeightInPx / 2);
	}

	/**
	 * Ship objects exhibit a number of special behaviors upon collisions. If a
	 * ship collides with a wormhole, it will teleport. If a ship collides with
	 * the gate, it will send a message that indicates the game has been won. If
	 * the ship collides with medicine, a life is restored, and if a ship
	 * collides with a regular object, a life (shield) is deducted.
	 */
	@Override
	protected void doCollideAction(SpaceActor s, float time) {
		if (s instanceof Wormhole) {
			// teleport only if the wormhole is a sending wormhole
			// if it is a receiving wormhole, the destination is null
			if (((Wormhole) s).getWormholeDestination() != null) {
				// we add the vector (1, 1) to destination position to avoid
				// gravity converging on infinity at the destination center
				this.setPosition(((Wormhole) s).getWormholeDestination()
						.getPosition().cpy().add(new Vector2(1, 1)));
			}
		} else if (s instanceof Gate) {
			win();
		} else {
			// collision with normal object
			boolean isMedicine = (s instanceof Medicine);
			if (isMedicine) {
				// No collision/change in motion if medicine.
				healthLevel++;
			} else {
				super.doCollideAction(s, time);
				healthLevel--;
			}
			// Clamp numRings in the range [-1, INITIAL_HEALTH_LEVEL]
			healthLevel = SimplePhysics.clamp(healthLevel, -1,
					INITIAL_HEALTH_LEVEL);
			if (healthLevel == -1)
				explode();
			else {
				updateTexture();
			}
		}
	}

	/**
	 * This method should provide automatic texture scaling to compensate for
	 * the fact that both the actual image dimensions and the proportion of the
	 * image occupied by the ship are changing. It should update the Object's
	 * preferred Texture height so that the ship itself within its own texture,
	 * remains a constant size. TODO: does not work correctly.
	 */
	private void updateTexture() {
		int oldWidth = getTexture().getWidth();
		texture = new Texture(
				Gdx.files.internal(Resources.images.SHIP[healthLevel]));
		setProportionScreenWidth(((float) getTexture().getWidth() / oldWidth)
				* proportionScreenWidth());
		updateTextureHeight();
	}

	/**
	 * Lose the level.
	 */
	private void explode() {
		status = LevelStatusManager.LevelStatus.LOSS;
	}

	/**
	 * WIn the level.
	 */
	private void win() {
		status = LevelStatusManager.LevelStatus.WIN;
	}

	/**
	 * A game may be IN_PROGRESS, WIN, or LOSS.
	 * 
	 * @return the status of the game
	 */
	public LevelStatusManager.LevelStatus getGameStatus() {
		return status;
	}

	/**
	 * Update the proportion of the width of the device screen that the Ship
	 * occupies.
	 * 
	 * @param widthProportion
	 *            the decimal proportion of the screen
	 */
	private void setProportionScreenWidth(float widthProportion) {
		WIDTH_PROPORTION = widthProportion;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Texture getTexture() {
		return texture;
	}
}
