package app.sunstreak.gravity.actor;

import app.sunstreak.gravity.util.Resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;

/**
 * The gate inherits its properties from its SpaceActor superclass, but is
 * treated specially as the exit point.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */

public class Gate extends Planet {

	public final float WIDTH_PROPORTION = .2f;
	public static final String JSON_NAME = "gate";

	/**
	 * Construct an instance of this Gate using specified properties.
	 * 
	 * @param startPos
	 *            the starting coordinates, from bottom-left
	 * @param startVel
	 *            the starting velocity, in x/y components
	 * @param mass
	 *            the mass
	 * @param imgFileHandle
	 *            the file path of an image that should be used as the texture
	 */
	public Gate(Vector2 startPos, Vector2 startVel, int mass,
			FileHandle imgFileHandle) {
		super (startPos, startVel, mass, imgFileHandle);
	}
	
	public Gate (Vector2 startPos, Vector2 startVel, int mass) {
		this (startPos, startVel, mass, Gdx.files.internal(Resources.images.EARTH));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float proportionScreenWidth() {
		return WIDTH_PROPORTION;
	}
}
