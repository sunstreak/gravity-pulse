package app.sunstreak.gravity.actor;

import app.sunstreak.gravity.actor.collide.CircleCollisionDetector;
import app.sunstreak.gravity.actor.collide.CollisionDetector;
import app.sunstreak.gravity.util.Resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

/**
 * Planet is a SpaceActor that exerts gravity and is not movable.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */
public class Planet extends SpaceActor {

	// The proportion of the screen's width that the texture should be scaled to
	// occupy
	public final float WIDTH_PROPORTION = .2f;
	public static final String JSON_NAME = "planet";

	private final Texture texture;
	private final float mass;

	/**
	 * Construct an instance of Planet.
	 * 
	 * @param startPos
	 *            the starting coordinates, from bottom-left
	 * @param startVel
	 *            the starting velocity, in x/y components
	 * @param mass
	 *            the mass
	 * @param imgFileHandle
	 *            the path to the Texture file
	 */
	public Planet(Vector2 startPos, Vector2 startVel, int mass,
			FileHandle imgFileHandle) {
		this.mass = mass;
		texture = new Texture(imgFileHandle);
		initialize(startPos, startVel);
	}
	
	public Planet(Vector2 startPos, Vector2 startVel, int mass) {
		this(startPos, startVel, mass, Gdx.files.internal(Resources.images.FREE_MOON));
	}

	/**
	 * {@inheritDoc} A planet is not movable.
	 */
	@Override
	public boolean isMovable() {
		return false;
	}

	/**
	 * {@inheritDoc} A planet has gravity.
	 */
	@Override
	public boolean hasGravity() {
		return true;
	}

	/**
	 * {@inheritDoc} A finger is not movable.
	 */
	@Override
	public float proportionScreenWidth() {
		return WIDTH_PROPORTION;
	}

	/**
	 * {@inheritDoc} A finger is not movable.
	 */
	@Override
	public CollisionDetector createCollisionDetector() {
		return new CircleCollisionDetector(getPosition(), textureWidthInPx / 2);
	}

	/**
	 * {@inheritDoc} A finger is not movable.
	 */
	@Override
	protected Texture getTexture() {
		return texture;
	}

	/**
	 * {@inheritDoc} A finger is not movable.
	 */
	@Override
	public float getMass() {
		return mass;
	}
}
