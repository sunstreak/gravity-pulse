package app.sunstreak.gravity.actor;

import app.sunstreak.gravity.actor.collide.CollisionDetector;
import app.sunstreak.gravity.backend.SimplePhysics;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * SpaceActor represents an Actor in the Grid. This class provides methods for
 * the actor to draw itself, maintain its own position and velocity, handle
 * collision results with default behaviors, and manage its force and motion for
 * physics computations.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */
public abstract class SpaceActor {

	public final float COEFFICIENT_OF_RESTITUTION = 0.85f;
	// Maximum directional component of velocity
	public final float MAX_VELOCITY_COMPONENT = .26f;

	private Vector2 position;
	private Vector2 velocity;

	protected float textureWidthInPx;
	protected float textureHeightInPx;
	protected float cornerCoordX;
	protected float cornerCoordY;
	private CollisionDetector collisionDetector;

	/**
	 * Can the Actor be moved or affected by gravity?
	 * 
	 * @return whether the Actor is movable
	 */
	public abstract boolean isMovable();

	/**
	 * Does the Actor generate gravity of its own?
	 * 
	 * @return whether the actor generates gravity
	 */
	public abstract boolean hasGravity();

	/**
	 * What proportion of the width of the device screen should the texture
	 * occupy?
	 * 
	 * @return a decimal proportion of screen width
	 */
	public abstract float proportionScreenWidth();

	/**
	 * Generate the appropriate CollisionDetector, based on the type and shape
	 * of the Actor.
	 * 
	 * @return the CollisionDetector object
	 */
	public abstract CollisionDetector createCollisionDetector();

	/**
	 * Must be the called in the constructors of implementations of SpaceActor
	 * after the texture is instantiated and set.
	 * 
	 * @param startPos
	 *            the starting position of the Actor, from bottom left
	 * @param startVel
	 *            the starting velocity of the Actor
	 * @param mass
	 *            the mass of the actor
	 */
	public void initialize(Vector2 startPos, Vector2 startVel) {
		position = startPos;
		velocity = startVel;

		updateTextureHeight();
		// Listens for collisions with other Actors
		collisionDetector = createCollisionDetector();
	}

	/**
	 * Update the CollisionDetector
	 * 
	 * @param collisionDetector
	 *            the new collisionDetector
	 */
	protected void setCollisionDetector(CollisionDetector collisionDetector) {
		this.collisionDetector = collisionDetector;
	}

	/**
	 * Draw this actor in the grid with its position at the center of the image.
	 * 
	 * @param sb
	 *            the SpriteBatch used to draw the Actor
	 */
	public void drawSelf(SpriteBatch sb) {
		sb.draw(getTexture(), position.x - (textureWidthInPx / 2), position.y
				- (textureHeightInPx / 2), textureWidthInPx, textureHeightInPx);
	}

	/**
	 * Get the actor's coordinates, from bottom-left
	 * 
	 * @return the position of the center of the Actor
	 */
	public Vector2 getPosition() {
		return position;
	}

	/**
	 * Get a vector describing the Actor's x/y velocity
	 * 
	 * @return the velocity vector
	 */
	public Vector2 getVelocity() {
		return velocity;
	}

	/**
	 * Returns the mass of this SpaceActor
	 * 
	 * @return the mass of this SpaceActor
	 */
	public abstract float getMass();

	/**
	 * Get the image for drawing
	 * 
	 * @return the Actor's Texture
	 */
	protected abstract Texture getTexture();

	/**
	 * Updates the velocity of this object given a force and a time interval.
	 * 
	 * @param force
	 *            the Gravitational force on this Actor
	 * @param time
	 *            the time elapsed since last calculation
	 */
	public void applyForce(Vector2 force, float time) {
		// Get changes in velocity using F = m dv/dt
		// Then, change the velocities to what they will be at the end of the
		// application of gravitational force over the given interval of time
		getVelocity().mulAdd(force, time / getMass());

		// Limit the velocity components to a maximum value
		getVelocity().x = SimplePhysics.clamp(getVelocity().x,
				-MAX_VELOCITY_COMPONENT, MAX_VELOCITY_COMPONENT);
		getVelocity().y = SimplePhysics.clamp(getVelocity().y,
				-MAX_VELOCITY_COMPONENT, MAX_VELOCITY_COMPONENT);
	}

	/**
	 * Moves position at current velocity for given time duration.
	 * 
	 * @param time
	 *            the time elapsed since last computation
	 */
	public void move(float time) {

		// Add the scalar product of velocity and time to position.
		// Velocity is not mutated.
		position.mulAdd(getVelocity(), time);

		if (isMovable())
			collisionDetector.setCenters(getPosition(), getPosition().cpy()
					.mulAdd(getVelocity(), time));
	}

	/**
	 * Updates the position coordinates, from bottom left
	 * 
	 * @param position
	 *            the new position
	 */
	protected void setPosition(Vector2 position) {
		this.position = position;
	}

	/**
	 * Called when the CollisionDetector indicates that this Actor has collided
	 * with another Actor. Checks the properties of both Actors involved, and
	 * performs the appropriate reciprocal collideActions for both actors
	 * (Newton's third law).
	 * 
	 * @param s
	 *            the other Actor
	 * @param time
	 *            the time elapsed since last computation
	 * @return whether or not the properties of each Actor allowed a
	 *         collideAction to be performed
	 */
	public boolean collide(SpaceActor s, float time) {
		if ((isMovable() || s.isMovable())
				&& collisionDetector.collide(s.collisionDetector)) {
			doCollideAction(s, time);
			s.doCollideAction(this, time);
			return true;
		}
		return false;
	}

	/**
	 * By default, movable objects exhibit imperfectly elastic rebound on each
	 * other with a coefficient of restitution. Override this to make objects
	 * behave differently upon collision.
	 * 
	 * @param s
	 *            the other SpaceActor
	 * @param time
	 *            the elapsed time since last computation
	 */
	protected void doCollideAction(SpaceActor s, float time) {
		if (isMovable()) {
			if (s.isMovable()) {
				float m1 = getMass();
				float m2 = s.getMass();

				Vector2 vCOM = getVelocity().cpy().lerp(s.getVelocity(),
						m2 / (m1 + m2));
				Vector2 u1 = getVelocity().cpy().sub(vCOM);

				// motion in both directions is reversed
				getVelocity().set(vCOM.sub(u1))
						.scl(-COEFFICIENT_OF_RESTITUTION);
			} else {
				Vector2 pointOfIntersection = collisionDetector
						.getPointOfIntersection(s.collisionDetector);
				Vector2 rHat = pointOfIntersection.cpy().sub(getPosition())
						.nor();
				Vector2 velocityInCollidingDirection = rHat.scl(rHat
						.dot(getVelocity()));
				getVelocity().mulAdd(velocityInCollidingDirection,
						-1 - COEFFICIENT_OF_RESTITUTION);
				/*
				 * When a movable object collides with a non-movable object,
				 * move the object by one extra tick to prevent the object from
				 * getting "stuck" inside the non-movable object. TODO: Improve.
				 */
				move(time);
			}
		}
	}

	/**
	 * Recomputes the dimensions of the Texture. This may be necessary if the
	 * proportionScreenWidth or the Texture itself changes.
	 */
	protected void updateTextureHeight() {

		Texture texture = getTexture();

		textureWidthInPx = SimplePhysics.SCRWIDTH * proportionScreenWidth();
		textureHeightInPx = textureWidthInPx * texture.getHeight()
				/ texture.getWidth();
	}

	/**
	 * Disposes of internal resources that are not garbage-collected on all
	 * platforms.
	 */
	public void dispose() {
		if (getTexture() != null) // did we beat the garbage collector?
			getTexture().dispose();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return String.format("%s position:%s, velocity:%s, mass:%.2f",
				getClass().getSimpleName(), position, velocity, getMass());
	}
}
