package app.sunstreak.gravity.actor;

import app.sunstreak.gravity.actor.collide.CircleCollisionDetector;
import app.sunstreak.gravity.actor.collide.CollisionDetector;
import app.sunstreak.gravity.actor.collide.UselessCollisionDetector;
import app.sunstreak.gravity.util.Resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * Medicine is a special SpaceActor that ignores all collisions until it
 * collides with the Ship. It is "consumed" by the Ship, and therefore
 * disappears. It is fixed and has gravity.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */
public class Medicine extends SpaceActor {

	public final float WIDTH_PROPORTION = .06f;
	public static final String JSON_NAME = "medicine";

	private Texture texture;
	private boolean exists;
	private final float mass;

	/**
	 * Construct an instance of Debris.
	 * 
	 * @param startPos
	 *            the starting coordinates, from bottom-left
	 * @param startVel
	 *            the starting velocity, in x/y components
	 * @param mass
	 *            the mass
	 */
	public Medicine(Vector2 startPos, Vector2 startVel, int mass, FileHandle imgFileHandle) {
		texture = new Texture(imgFileHandle);
		this.mass = mass;
		initialize(startPos, startVel);
		exists = true;
	}
	
	public Medicine(Vector2 startPos, Vector2 startVel, int mass) {
		this(startPos, startVel, mass, Gdx.files.internal(Resources.images.MEDICINE));
	}

	/**
	 * {@inheritDoc} Medicine is not movable.
	 */
	@Override
	public boolean isMovable() {
		return false;
	}

	/**
	 * {@inheritDoc} Medicine has gravity when it is displayed.
	 */
	@Override
	public boolean hasGravity() {
		return exists;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float getMass() {
		return mass;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float proportionScreenWidth() {
		return WIDTH_PROPORTION;
	}

	/**
	 * {@inheritDoc} Medicine has a circular collision detector.
	 */
	@Override
	public CollisionDetector createCollisionDetector() {
		return new CircleCollisionDetector(getPosition(), textureWidthInPx / 2);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void drawSelf(SpriteBatch batch) {
		if (exists)
			super.drawSelf(batch);
	}

	/**
	 * Medicine only performs an action upon colliding with a Ship. If it
	 * collides with a Ship, the Ship will restore its own life and Medicine
	 * will disappear.
	 */
	@Override
	protected void doCollideAction(SpaceActor s, float time) {
		if (s instanceof Ship) {
			exists = false;

			// once it it used, it disappears
			texture.dispose();
			texture = null;
			setCollisionDetector(new UselessCollisionDetector());
		} else {
			// Do nothing.
			// Medicine does not do anything when it collides with
			// another object.
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Texture getTexture() {
		return texture;
	}
}
