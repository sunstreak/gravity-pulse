package app.sunstreak.gravity.actor;

import app.sunstreak.gravity.actor.collide.CircleCollisionDetector;
import app.sunstreak.gravity.actor.collide.CollisionDetector;
import app.sunstreak.gravity.util.Resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

/**
 * Debris is a type of SpaceActor that is free-floating and exerts gravity.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */
public class Debris extends SpaceActor {

	// The proportion of the screen's width that the texture should be scaled to
	// occupy
	public final float WIDTH_PROPORTION = .05f;
	public static final String JSON_NAME = "debris";

	private final float mass;
	private final Texture texture;

	/**
	 * Construct an instance of Debris.
	 * 
	 * @param startPos
	 *            the starting coordinates, from bottom-left
	 * @param startVel
	 *            the starting velocity, in x/y components
	 * @param mass
	 *            the mass
	 */
	public Debris(Vector2 startPos, Vector2 startVel, int mass, FileHandle imageFileHandle) {
		texture = new Texture(imageFileHandle);
		this.mass = mass;
		initialize(startPos, startVel);
	}
	
	public Debris(Vector2 startPos, Vector2 startVel, int mass) {
		this(startPos, startVel, mass, Gdx.files.internal(Resources.images.DEBRIS));
	}

	/**
	 * {@inheritDoc} Debris can move.
	 */
	@Override
	public boolean isMovable() {
		return true;
	}

	/**
	 * {@inheritDoc} Debris has gravity.
	 */
	@Override
	public boolean hasGravity() {
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float proportionScreenWidth() {
		return WIDTH_PROPORTION;
	}

	/**
	 * {@inheritDoc} Debris has a circular collision detector.
	 */
	@Override
	public CollisionDetector createCollisionDetector() {
		return new CircleCollisionDetector(getPosition(), textureWidthInPx / 2);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Texture getTexture() {
		return texture;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float getMass() {
		return mass;
	}
}
