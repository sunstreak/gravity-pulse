package app.sunstreak.gravity.actor.collide;

import app.sunstreak.gravity.util.Rectangle;

import com.badlogic.gdx.math.Vector2;

/**
 * A UselessCollisionDetector is a CollisionDetector that never collides with
 * anything. When asked if it collides with something, it always returns false.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */
public class UselessCollisionDetector implements CollisionDetector {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean collide(CollisionDetector other) {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCenters(Vector2 position, Vector2 futurePosition) {
		// Do nothing.
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Vector2 getPointOfIntersection(CollisionDetector other) {
		throw new RuntimeException("No collision has occurred.\n"
				+ "This method should only be called after a collision"
				+ "has occurred.");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Rectangle getBoundingRectangle() {
		// Since this object doesn't really exist,
		// it is bounded by a Rectangle of no size.
		return new Rectangle(0, 0, 0, 0);
	}
}
