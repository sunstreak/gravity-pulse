package app.sunstreak.gravity.actor.collide;

import app.sunstreak.gravity.util.Rectangle;

import com.badlogic.gdx.math.Vector2;

/**
 * A CircleCollisionDetector is a CollisionDetector for a circular object.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */

public class CircleCollisionDetector implements CollisionDetector {

	private Vector2 center;
	private Vector2 futureCenter;
	private final float radius;
	private final Rectangle boundingRectangle;

	public CircleCollisionDetector(Vector2 center, float radius) {
		this.center = center;
		this.futureCenter = center.cpy();
		this.radius = radius;
		boundingRectangle = new Rectangle((int) (center.x - radius),
				(int) (center.y - radius), (int) radius * 2, (int) radius * 2);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean collide(CollisionDetector other) {
		if (other instanceof CircleCollisionDetector) {
			return collidesWithCircle((CircleCollisionDetector) other);
		} else if (other instanceof RectangleCollisionDetector) {
			return collidesWithRectangle((RectangleCollisionDetector) other);
		} else if (other instanceof UselessCollisionDetector) {
			return other.collide(this);
		} else if (other == null) {
			throw new NullPointerException("collisionDetector is null.");
		} else {
			throw new RuntimeException(getClass().getName()
					+ " does not know how to collide with "
					+ other.getClass().getName());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCenters(Vector2 position, Vector2 futurePosition) {
		center = position;
		this.futureCenter = futurePosition;
		boundingRectangle.setLocation((int) (position.x - radius),
				(int) (position.y - radius));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Vector2 getPointOfIntersection(CollisionDetector other) {
		if (other instanceof CircleCollisionDetector)
			return center
					.cpy()
					.lerp(((CircleCollisionDetector) other).center,
							radius
									/ (radius + ((CircleCollisionDetector) other).radius));
		else
			return other.getPointOfIntersection(this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Rectangle getBoundingRectangle() {
		return boundingRectangle;
	}

	/**
	 * Determine whether this CollisionDetector collides with that of another
	 * circular object using multiple positions.
	 * 
	 * @param other
	 *            the CollisionDetector of another circular object
	 * @return whether the two objects collide
	 */
	private boolean collidesWithCircle(CircleCollisionDetector other) {
		return circlesIntersect(center, radius, other.center, other.radius)
				|| circlesIntersect(futureCenter, radius, other.futureCenter,
						other.radius);
	}

	/**
	 * Determine whether this bounding circle intersects that of another
	 * circular object.
	 * 
	 * @param center1
	 *            Center coordinates of this circle
	 * @param radius1
	 *            Radius of this circle
	 * @param center2
	 *            Center coordinates of the other circle
	 * @param radius2
	 *            Radius of the other circle
	 * @return whether the circles intersect
	 */
	private static boolean circlesIntersect(Vector2 center1, float radius1,
			Vector2 center2, float radius2) {
		// Calculate distance between the two circle centers
		float dist2 = center1.dst2(center2);
		float sumOfRadii = radius1 + radius2;

		// If this distance is less then the sum of the radii, they intersect.
		return dist2 <= sumOfRadii * sumOfRadii;
	}

	/**
	 * Determine whether this CollisionDetector collides with that of a
	 * rectangular object using multiple positions.
	 * 
	 * @param other
	 *            the CollisionDetector of another circular object
	 * @return whether the two objects collide
	 */
	private boolean collidesWithRectangle(RectangleCollisionDetector other) {
		return getBoundingRectangle().intersects(other.getBoundingRectangle());
	}

}
