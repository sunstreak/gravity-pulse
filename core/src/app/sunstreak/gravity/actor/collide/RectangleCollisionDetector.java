package app.sunstreak.gravity.actor.collide;

import app.sunstreak.gravity.util.Rectangle;

import com.badlogic.gdx.math.Vector2;

/**
 * A RectangleCollisionDetector is a CollisionDetector for a rectangular object.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */

public class RectangleCollisionDetector implements CollisionDetector {

	private final Rectangle rect;

	/**
	 * Instantiate the RectangleCollisionDetector for particular dimensions at a
	 * particular position.
	 * 
	 * @param topLeft
	 *            the coordinates, from bottom-left, of the upper left corner
	 * @param size
	 *            the size (x,y) dimensions of the bounding rectangle
	 */
	public RectangleCollisionDetector(Vector2 topLeft, Vector2 size) {
		rect = new Rectangle((int) topLeft.x, (int) topLeft.y, (int) size.x,
				(int) size.y);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean collide(CollisionDetector other) {
		if (other instanceof RectangleCollisionDetector) {
			return rect.intersects(((RectangleCollisionDetector) other).rect);
		} else if (other instanceof CircleCollisionDetector) {
			return other.collide(this);
		} else if (other instanceof UselessCollisionDetector) {
			return other.collide(this);
		} else {
			throw new RuntimeException(getClass().getName()
					+ " does not know how to collide with "
					+ other.getClass().getName());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCenters(Vector2 position, Vector2 futurePosition) {
		rect.setLocation(position.x - rect.width / 2, position.y - rect.height
				/ 2);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Vector2 getPointOfIntersection(CollisionDetector other) {
		Rectangle intersection = getBoundingRectangle().createIntersection(
				other.getBoundingRectangle());
		return new Vector2(intersection.getX() + intersection.width / 2,
				intersection.getY() + intersection.height / 2);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Rectangle getBoundingRectangle() {
		return rect;
	}

}
