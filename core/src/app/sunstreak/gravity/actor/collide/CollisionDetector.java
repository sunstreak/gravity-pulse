package app.sunstreak.gravity.actor.collide;

import app.sunstreak.gravity.util.Rectangle;

import com.badlogic.gdx.math.Vector2;

/**
 * The CollisionDetector interface represents a shape in 2-D space than can
 * collide with other CollisionDetectors.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */

public interface CollisionDetector {

	/**
	 * Calculates whether this <code>CollisionDetector</code> is currently
	 * colliding with another <code>CollisionDetector</code>.
	 * 
	 * @param other
	 *            another <code>CollisionDetector</code>
	 * @return whether a collision has occurred
	 */
	public boolean collide(CollisionDetector other);

	/**
	 * Change the position of this CollisionDetector.
	 * 
	 * @param position
	 *            the current position of the <code>CollisionDetector</code>.
	 * @param futurePosition
	 *            where the <code>CollisionDetector</code> will probably be in
	 *            one frame.
	 */
	public void setCenters(Vector2 position, Vector2 futurePosition);

	/**
	 * Returns the center of the overlapped region between this
	 * <code>CollisionDetector</code> and another <code>CollisionDetector</code>
	 * .
	 * 
	 * PRECONDITION: this <code>CollisionDetector</code> actually collided with
	 * the other <code>CollisionDetector</code> (i.e. the collide method
	 * returned true.)
	 * 
	 * @param other
	 *            another CollisionDetector
	 * @return
	 */
	public Vector2 getPointOfIntersection(CollisionDetector other);

	/**
	 * Get the rectangle that bounds this CollisionDetector.
	 * 
	 * @return the bounding rectangle
	 */
	public Rectangle getBoundingRectangle();

}
