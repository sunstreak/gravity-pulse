package app.sunstreak.gravity.actor;

import app.sunstreak.gravity.util.Resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;

/**
 * Repulsor is a Planet that repels objects instead of attracting them. It has a
 * "negative" mass.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */
public class Repulsor extends Planet {

	public final float WIDTH_PROPORTION = .2f;
	public static final String JSON_NAME = "repulsor";

	/**
	 * Construct an instance of a Repulsor. The Repulsor pushes nearby objects
	 * away.
	 * 
	 * @param startPos
	 *            the starting coordinates, from bottom-left
	 * @param startVel
	 *            the starting velocity, in x/y components
	 * @param mass
	 *            the mass
	 */
	public Repulsor(Vector2 startPos, Vector2 startVel, int mass) {
		this(startPos, startVel, mass, Gdx.files.internal(Resources.images.REPULSOR));
	}
	
	public Repulsor(Vector2 startPos, Vector2 startVel, int mass, FileHandle imgFileHandle) {
		super(startPos, startVel, mass, imgFileHandle);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float proportionScreenWidth() {
		return WIDTH_PROPORTION;
	}
}
