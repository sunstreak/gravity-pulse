package app.sunstreak.gravity.actor;

import java.util.ArrayList;
import java.util.Collection;

import app.sunstreak.gravity.actor.collide.CircleCollisionDetector;
import app.sunstreak.gravity.actor.collide.CollisionDetector;
import app.sunstreak.gravity.util.Resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

/**
 * Wormholes exist in pairs. The Ship handles its own teleportation through
 * Wormholes. This class provides custom methods to handle the relationship
 * between paired Wormholes.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */
public class Wormhole extends SpaceActor {

	/**
	 * Wormholes must be wither senders or receivers. The Ship travels through
	 * them.
	 */
	public enum Type {
		SOURCE, DESTINATION;
	}

	public final float WIDTH_PROPORTION = .11f;

	// If the wormhole is a sending wormhole, provide a wormhole reference.
	// If the wormhole is a receiving wormhole, this should be null.
	// Wormholes must be either sending or receiving, not both.
	private Wormhole partner;

	private final Texture texture;
	private final Type type;
	private final float mass;

	/**
	 * Construct an instance of this Wormhole using specified properties.
	 * 
	 * @param startPos
	 *            the starting coordinates, from bottom-left
	 * @param startVel
	 *            the starting velocity, in x/y components
	 * @param mass
	 *            the mass
	 * @param type
	 *            the type of the wormhole: source or destination
	 */
	private Wormhole(Vector2 startPos, int mass, Type type) {
		this.type = type;
		this.mass = mass;
		texture = new Texture(Gdx.files.internal(Resources.images.WORMHOLE));

		final Vector2 velocity = Vector2.Zero;
		initialize(startPos, velocity);
	}

	/**
	 * {@inheritDoc} Wormholes are not movable
	 */
	@Override
	public boolean isMovable() {
		return false;
	}

	/**
	 * {@inheritDoc} Only source wormholes have gravity
	 */
	@Override
	public boolean hasGravity() {
		return type == Type.SOURCE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float getMass() {
		return mass;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float proportionScreenWidth() {
		return WIDTH_PROPORTION;
	}

	/**
	 * Fetch the destination Wormhole associated with a source.
	 * 
	 * @return the destination Wormhole
	 */
	public Wormhole getWormholeDestination() {
		// TODO This method is a problem!
		// Returning null is bad.
		// This method shouldn't even be called if it's not a source.
		// Also, this method should probably be called "getPartner"
		switch (type) {
		case DESTINATION:
			return partner;
		default:
			return null;
		}
	}

	/**
	 * {@inheritDoc} A wormhole has a circular collision detector.
	 */
	@Override
	public CollisionDetector createCollisionDetector() {
		return new CircleCollisionDetector(getPosition(), textureWidthInPx / 2);
	}

	/**
	 * Wormholes do nothing upon collisions.
	 */
	@Override
	protected void doCollideAction(SpaceActor s, float time) {
		// Do nothing.
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Texture getTexture() {
		return texture;
	}

	/**
	 * Generate a matched pair of Wormholes.
	 * 
	 * @param sourcePosition
	 *            the coordinates, from bottom-left, of the source wormhole
	 * @param destinationPosition
	 *            the coordinates of the destination Wormhole
	 * @param sourceMass
	 *            the mass of the source
	 * @param destinationMass
	 *            the mass of the destination
	 * @return a Collection containing the two Wormholes
	 */
	public static Collection<Wormhole> createWormholePair(
			Vector2 sourcePosition, Vector2 destinationPosition,
			int sourceMass, int destinationMass) {

		Wormhole source = new Wormhole(sourcePosition, sourceMass, Type.SOURCE);
		Wormhole destination = new Wormhole(destinationPosition,
				destinationMass, Type.DESTINATION);

		source.partner = destination;
		destination.partner = source;

		ArrayList<Wormhole> wormholes = new ArrayList<Wormhole>();
		wormholes.add(source);
		wormholes.add(destination);

		return wormholes;
	}
}
