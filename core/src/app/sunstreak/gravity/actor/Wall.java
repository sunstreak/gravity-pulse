package app.sunstreak.gravity.actor;

import app.sunstreak.gravity.actor.collide.CollisionDetector;
import app.sunstreak.gravity.actor.collide.RectangleCollisionDetector;
import app.sunstreak.gravity.util.Resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * A Wall is a SpaceActor that has not gravity and cannot move. Other objects
 * may collide with it. It uses a special drawing routine independent of the
 * width proportion mechanism.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */
public class Wall extends SpaceActor {

	public static final int BOUNDING_WIDTH = 20;
	private final float width;
	private final float height;

	private final Texture texture;

	/**
	 * Construct an instance of this Wall using specified properties.
	 * 
	 * @param startPos
	 *            the starting coordinates, from bottom-left
	 * @param width
	 *            the x-dimension of the wall
	 * @param height
	 *            the y-dimension of the wall
	 */
	public Wall(Vector2 startPos, float width, float height) {
		this.width = width;
		this.height = height;
		// The wall texture is intended to be stretched so that it appears to be
		// a "forcefield"
		texture = new Texture(Gdx.files.internal(Resources.images.WALL));
		initialize(startPos, new Vector2(0, 0));
	}

	/**
	 * Draw the wall centered on its position.
	 * 
	 * @param sb
	 *            the SpriteBatch that draws on the UI
	 */
	@Override
	public void drawSelf(SpriteBatch sb) {
		// draw the image centered on its position
		sb.draw(getTexture(), getPosition().x, getPosition().y, width, height);
	}

	/**
	 * {@inheritDoc} Wall cannot move.
	 */
	@Override
	public boolean isMovable() {
		return false;
	}

	/**
	 * {@inheritDoc} Wall does not have gravity.
	 */
	@Override
	public boolean hasGravity() {
		return false;
	}

	/**
	 * {@inheritDoc} Wall has no mass.
	 */
	@Override
	public float getMass() {
		return 0;
	}

	/**
	 * {@inheritDoc} Wall has a rectangular collision detector.
	 */
	@Override
	public CollisionDetector createCollisionDetector() {
		Vector2 topLeft = new Vector2(getPosition().x, getPosition().y);
		return new RectangleCollisionDetector(topLeft, new Vector2(width,
				height));
	}

	/**
	 * {@inheritDoc} For Wall, this always returns zero.
	 */
	@Override
	public float proportionScreenWidth() {
		return 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Texture getTexture() {
		return texture;
	}

}
