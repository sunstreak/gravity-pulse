package app.sunstreak.gravity.actor;

import app.sunstreak.gravity.actor.collide.CollisionDetector;
import app.sunstreak.gravity.actor.collide.UselessCollisionDetector;
import app.sunstreak.gravity.util.Resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * An InvisibleGravitator is a special SpaceActor used to represent gravity
 * exerted by a finger touch. It provides special methods to activate and
 * deactivate both its gravity and its image, and it exhibits special behavior
 * for the purposes of physics calculations.
 * 
 * @author Dylan Hunn
 * @author Sidharth Kaupur
 * @author Han Li
 */
public class InvisibleGravitator extends SpaceActor {

	private final float MASS = 500;
	public final float WIDTH_PROPORTION = .1f;
	boolean exists;

	private final Texture texture;

	// the limit, in pixels, for how close to the ship gravity may be applied
	public final int MINIMUM_GRAVITATE_DISTANCE = 30;

	/**
	 * Construct an instance of this SpaceActor.
	 */
	public InvisibleGravitator() {
		exists = false;
		texture = new Texture(Gdx.files.internal(Resources.images.RIPPLE));
		initialize(Vector2.Zero, Vector2.Zero);
	}

	/**
	 * {@inheritDoc} A finger is not movable.
	 */
	@Override
	public boolean isMovable() {
		return false;
	}

	/**
	 * {@inheritDoc} A finger has gravity.
	 */
	@Override
	public boolean hasGravity() {
		return exists;
	}

	/**
	 * Gravity on a SpaceActor is not calculated if that Actor is less than a
	 * threshold distance from the finger.
	 * 
	 * @param other
	 *            the other SpaceActor
	 * @return whether or not to exert gravity
	 */
	public boolean tooCloseTo(SpaceActor other) {
		return getPosition().dst2(other.getPosition()) < MINIMUM_GRAVITATE_DISTANCE
				* MINIMUM_GRAVITATE_DISTANCE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float getMass() {
		return MASS;
	}

	/**
	 * Activate the touch. Calling this method enables gravity and image
	 * drawing, etc.
	 * 
	 * @param touchPos
	 *            the coordinates of the touch, from bottom-left
	 */
	public void on(Vector2 touchPos) {
		exists = true;
		setPosition(touchPos);
	}

	/**
	 * Deactivate the gravity and image of the touch.
	 */
	public void off() {
		exists = false;
	}

	/**
	 * {@inheritDoc} Only draw the finger's texture if the screen is presently
	 * touched.
	 */
	@Override
	public void drawSelf(SpriteBatch sb) {
		if (exists)
			super.drawSelf(sb);
		// Invisible. Therefore, no need to draw.
		return;
	}

	/**
	 * {@inheritDoc} A finger does not need collision detection.
	 */
	@Override
	public CollisionDetector createCollisionDetector() {
		return new UselessCollisionDetector();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float proportionScreenWidth() {
		return WIDTH_PROPORTION;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Texture getTexture() {
		return texture;
	}

}
